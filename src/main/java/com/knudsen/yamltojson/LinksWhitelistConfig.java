package com.knudsen.yamltojson;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class LinksWhitelistConfig {
    
    private List<Link> linksWhitelist = new ArrayList<>();
    
    public List<Link> getLinksWhitelist() {
        return this.linksWhitelist;
    }
}
