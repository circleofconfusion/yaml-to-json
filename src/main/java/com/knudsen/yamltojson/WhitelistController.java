package com.knudsen.yamltojson;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class WhitelistController {
    
    @Autowired
    LinksWhitelistConfig linksWhitelistConfig;
    
    
    @RequestMapping(value = "/links-whitelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Link> getWhitelist() {
        return linksWhitelistConfig.getLinksWhitelist();
    }
}
