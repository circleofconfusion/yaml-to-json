# YAML config to JSON output test

This is an example of exposing configuration data in application.yml to HTTP GET
requests via a Spring Boot REST Controller.

## Build

Install gradle. I had to go the the Gradle download page and manually install
it in CentOS, and then add the bin directory to my PATH.

With Gradle installed, run:

    gradle bootJar
    java -jar build/libs/yaml-to-json.jar
